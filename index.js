const fs = require('fs')
const path = require('path')

module.exports = function(options) {
    options = options || {};
    options.root = options.root || '.';

    /**
     * Copy assets
     *
     * @param {*} tree
     * @returns {*}
     */
    function posthtmlAssets(tree) {
        // https://www.w3schools.com/tags/att_src.asp

        // TODO img srcset
        tree.match([
            { tag: 'audio' },
            { tag: 'embed' },
            { tag: 'img' },
            { tag: 'source' },
            { tag: 'track ' },
            { tag: 'video' }
        ], function(node) {
            node.attrs = node.attrs || {}

            if (!node.attrs.src) {
                return node
            }

            fs.copyFileSync(
                path.join(
                    path.dirname(
                        path.relative(process.cwd(), tree.options.from)
                    ),
                    node.attrs.src
                ),
                path.join(
                    options.root,
                    node.attrs.src
                )
            )

            return node
        })

        return tree
    }

    return posthtmlAssets
}